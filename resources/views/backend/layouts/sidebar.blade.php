<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3> </div>
        <div class="user-profile">
            
        </div>
        <ul class="nav" id="side-menu">
            <li> <a href="{{ route('panel-dashboard') }}" class="waves-effect"><i class="mdi mdi-av-timer fa-fw" data-icon="v"></i> <span class="hide-menu"> Dashboard </span></a>
            </li>
            <li class="devider"></li>
             <li> <a href="{{ url('panel/website') }}" class="waves-effect"><i class="mdi mdi-laptop fa-fw" data-icon="v"></i> <span class="hide-menu"> Manage Webiste <span class="fa arrow"></span> </span></a>
                <ul class="nav nav-second-level">
                    <li><a href="{{ route('panel-website-profil') }}"><i data-icon="&#xe026;" class="mdi mdi-arrow-right-bold fa-fw"></i> <span class="hide-menu">Webstie Profile</span></a></li>
                    <li><a href="{{ route('panel-user-permission') }}"><i data-icon="&#xe025;" class="mdi mdi-arrow-right-bold fa-fw"></i> <span class="hide-menu">Website Media</span></a></li>
                </ul>
            </li>
            @if(!empty(Session::get('permission')['manage_user']))
            <li> <a href="{{ url('panel/user') }}" class="waves-effect"><i class="mdi mdi-account-multiple fa-fw"></i> <span class="hide-menu">Manage User<span class="fa arrow"></span> </span></a>
                <ul class="nav nav-second-level">
                    <li><a href="{{ route('panel-user') }}"><i data-icon="&#xe026;" class="mdi mdi-arrow-right-bold fa-fw"></i> <span class="hide-menu">User</span></a></li>
                    <li><a href="{{ route('panel-user-permission') }}"><i data-icon="&#xe025;" class="mdi mdi-arrow-right-bold fa-fw"></i> <span class="hide-menu">Role & Permission</span></a></li>
                </ul>
            </li>
            @endif
            <li class="devider"></li>
            <li><a href="javascript:void(0)" data-toggle="modal" data-target="#modalLogout" class="waves-effect"><i class="mdi mdi-logout fa-fw"></i> <span class="hide-menu">Log out</span></a></li>
            <li class="devider"></li>
            <li><a href="documentation.html" class="waves-effect"><i class="fa fa-circle-o text-danger"></i> <span class="hide-menu">Documentation</span></a></li>
        </ul>
    </div>
</div>
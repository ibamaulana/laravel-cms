<form id="form-editpermission" enctype="multipart/form-data" method="POST">
{{ csrf_field() }}
    <div class="modal-body">
        <div class="alert alert-danger" id="alert-error" role="alert" style="display: none">
          Error ! <div id="message-error"></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Role Name :</label>
                    <input type="text" name="role_name" class="form-control" required value="{{ $rolepermission->data[0]->name }}">
                </div>
            </div>
            <div class="col-md-12">
                <label>Permission :</label>
                @foreach($permission->data as $permission)
                <div class="checkbox">
                    <input id="checkbox{{ $permission->id }}" type="checkbox" name="permission[]" value="{{ $permission->name }}"  @foreach($rolepermission->data[0]->permissions as $permissionrole) {{ $permissionrole->id == $permission->id ? "checked" : "" }} @endforeach>
                    <label for="checkbox{{ $permission->id }}" style="min-height: 35px;">{{ $permission->name }} </label>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="modal-footer ">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-info">Update</button>
    </div>
</form>
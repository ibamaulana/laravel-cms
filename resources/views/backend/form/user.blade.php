<form id="form-user" enctype="multipart/form-data" method="POST">
{{ csrf_field() }}
    <div class="modal-body">
        <div class="alert alert-danger" id="alert-error" role="alert" style="display: none">
          Error ! <div id="message-error"></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Name :</label>
                    <input type="text" name="name" class="form-control" required>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Email :</label>
                    <input type="email" name="email" class="form-control" required>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Password :</label>
                    <input type="password" name="password" class="form-control" required>
                </div>
            </div>
            <div class="col-md-6">
                <label>Roles :</label>
                @foreach($roles->data as $role)
                <div class="checkbox">
                    <input id="checkbox{{ $role->id }}" type="checkbox" name="role[]" value="{{ $role->name }}">
                    <label for="checkbox{{ $role->id }}" style="min-height: 25px;">{{ $role->name }} </label>
                </div>
                @endforeach
            </div>
            <div class="col-md-6">
                <label>Permission :</label>
                @foreach($permission->data as $permission)
                <div class="checkbox">
                    <input id="checkbox{{ $permission->id }}" type="checkbox" name="permission[]" value="{{ $permission->name }}">
                    <label for="checkbox{{ $permission->id }}" style="min-height: 25px;">{{ $permission->name }} </label>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="modal-footer ">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-info">Create</button>
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function() {
        $('#form-user').on('submit',function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'{{ route('user-create') }}',
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $('.load-data').css('display','block');
                    $('.form-data').css('display','none');
                },
                success:function(data){
                    if(data['data']['error'] == null){
                        $('.load-data').css('display','none');
                        $('.form-data').css('display','block');
                        $('#modalCreate').modal('hide');
                        $('#alert-success').css('display','block');
                        getTabel('{{ route('user-get') }}',[],'GET');
                        return true;
                    }else{
                        $('.load-data').css('display','none');
                        $('.form-data').css('display','block');
                        $('#alert-error').css('display','block');
                        $('#message-error').html(data['data']['error']['message']);
                        
                        return true;
                    }

                }

            });
            return false;
        });
        $("#prod_image").change(function() {
            readURL(this);
        });
    });
</script>
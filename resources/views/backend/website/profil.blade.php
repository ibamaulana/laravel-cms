@extends('backend.layouts.app')
@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{{ $title }}</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li class="active">{{ $title }}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- ============================================================== -->
        <!-- Sales different chart widgets -->
        <!-- ============================================================== -->
        <!-- .row -->
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="white-box">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-8">
                                <h3 class="box-title m-l-10 m-t-5">WEBSITE PROFILE</h3>
                            </div>
                            <div class="col-md-4 text-right">
                                <a data-toggle="modal" data-target="#modalCreate" class="formButton"><button type="button" class="btn btn-success btn-rounded m-r-10"><i class="fa fa-refresh " style="margin-right: 10px"></i>  UPDATE</button></a>
                            </div>
                        </div>
                        <hr>
                        <div class="col-md-12">
                            <form>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Website Name :</label>
                                        <input type="text" name="web_name" class="form-control" placeholder="Enter your website name..." value="{{ $data->data->web_name }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tagline :</label>
                                        <input type="text" name="web_tagline" class="form-control" placeholder="Enter your website tagline..." value="{{ $data->data->web_tagline }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Website Description :</label>
                                        <textarea type="text" name="web_description" class="form-control" placeholder="Enter your website description..." rows="10">{{ $data->data->web_description }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Email :</label>
                                        <input type="email" name="web_email" class="form-control" placeholder="Enter your website email..." value="{{ $data->data->web_email }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Phone :</label>
                                        <input type="text" name="web_phone" class="form-control" placeholder="Enter your website phone..." value="{{ $data->data->web_phone }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Address :</label>
                                        <input type="text" name="web_address" class="form-control" placeholder="Enter your website address..." value="{{ $data->data->web_address }}">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.modal.user')
    <!-- /.container-fluid -->
    <footer class="footer text-center"> 2018 &copy; Laravel Content Management System </footer>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    function getTabel(url,datas,type){
        $.ajax({
            url: url,
            data: datas,
            type: type,
            beforeSend: function(){
                $('#load-data').css('display','block');
                $('#tabel-data').css('display','none');
            },
            success: function(data){
                $('#load-data').css('display','none');
                $('#tabel-data').css('display','block');
                $('#tabel-data').html(data);
            }
        });
    }
    $(document).ready(function() {
        {{-- getTabel('{{ route('user-get') }}',[],'GET'); --}}
        //on click jquery
        $('.formButton').on('click',function(){
            var id = $(this).attr('data-id');
            $.ajax({
                url: '{{ route('user-form-create') }}',
                data: {
                    id : id
                },
                type: 'GET',
                beforeSend: function(){
                    $('.modal-title').html('Create Product');
                    $('.load-data').css('display','block');
                    $('.form-data').css('display','none');
                },
                success: function(data){
                    $('.load-data').css('display','none');
                    $('.form-data').css('display','block');
                    $('.form-data').html(data);
                }
            });
        });
        $('#delete-user').on('click',function(){
            $.ajax({
                url: '{{ route('user-delete') }}',
                data: {
                    id : dataid
                },
                type: 'GET',
                beforeSend: function(){
                    $('.load-delete').css('display','block');
                    $('.form-delete').css('display','none');
                },
                success: function(data){
                    $('.load-delete').css('display','none');
                    $('.form-delete').css('display','block');
                    $('#modalDelete').modal('hide');
                    $('#alert-success').css('display','block');
                    getTabel('{{ route('user-get') }}',[],'GET');
                    return true;
                }
            });
        });
    });
</script>
@endpush
<div class="table-responsive">
    <table class="table table-bordered">
    	<thead>
	        <tr>
	        	<th class="text-center">No.</th>
	        	<th class="text-center">Name</th>
	        	<th class="text-center">Email</th>
	        	<th class="text-center">Role</th>
	        	<th class="text-center">Confirmation</th>
	        	<th class="text-center">Date Modified</th>
	        	<th class="text-center">Action</th>
	        </tr>
	    </thead>
	    <tbody>
	    	@php
	    		$no = 0;
	    	@endphp
	    	@foreach($data as $key)
	    	@php
	    		$no++;
	    	@endphp
		    	<tr>
		    		<td class="text-center">{{ $no }}.</td>
		    		<td class="text-center">{{ $key->name }}</td>
		    		<td class="text-center">{{ $key->email }}</td>
		    		<td class="text-left">
		    			<ul class="list-icons">
		    				@foreach($key->roles as $role)
		    					<li style="line-height: 0;"><i class="fa fa-caret-right text-info" ></i> {{ $role->name }}</li>
		    				@endforeach
		    			</ul>
		    		</td>
		    		<td class="text-center">
		    			@if($key->confirm == null)
		    				<span class="label label-danger">Unconfirmed</span>
		    			@else
		    				<span class="label label-success">Confirmed</span>
		    			@endif
		    		</td>
		    		<td class="text-center">{{ date('d-m-Y', strtotime($key->updated_at)) }}</td>
		    		<td class="text-center">
		    			<a class="edit" data-toggle="modal" data-target="#modalCreate" data-id={{ $key->id }}><button type="button" class="btn btn-warning btn-circle btn-sm m-r-5"><i class="ti-pencil-alt"></i></button></a>
                        <a class="delete" data-toggle="modal" data-target="#modalDelete" data-id={{ $key->id }}><button type="button" class="btn btn-danger  btn-circle btn-sm m-r-5"><i class="ti-trash"></i></button></a>
		    		</td>
		    	</tr>
	    	@endforeach
	    </tbody>
    </table>
</div>
<script type="text/javascript">
	$('.edit').on('click',function (){
		var id = $(this).attr('data-id');
        $.ajax({
            url: '{{ route('user-form-edit') }}',
            data: {
                id : id
            },
            type: 'GET',
            beforeSend: function(){
                $('.modal-title').html('Edit User');
                $('.load-data').css('display','block');
                $('.form-data').css('display','none');
            },
            success: function(data){
                $('.load-data').css('display','none');
                $('.form-data').css('display','block');
                $('.form-data').html(data);

            }
        });
	});
	$('.delete').on('click',function (){
        dataid = $(this).attr('data-id');
	});
</script>
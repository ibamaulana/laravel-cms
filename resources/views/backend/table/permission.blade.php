<div class="table-responsive">
    <table class="table table-bordered">
    	<thead>
	        <tr>
	        	<th class="text-center">No.</th>
	        	<th class="text-center">Permission</th>
	        	<th class="text-center">Action</th>
	        </tr>
	    </thead>
	    <tbody>
	    	@php
	    		$no = 0;
	    	@endphp
	    	@foreach($data as $key)
	    	@php
	    		$no++;
	    	@endphp
		    	<tr>
		    		<td class="text-center">{{ $no }}.</td>
		    		<td class="text-center">{{ $key->name }}</td>
		    		<td class="text-center">
                        <a class="delete" data-toggle="modal" data-target="#modalDeletePermission" data-id={{ $key->id }}><button type="button" class="btn btn-danger  btn-circle btn-sm m-r-5"><i class="ti-trash"></i></button></a>
		    		</td>
		    	</tr>
	    	@endforeach
	    </tbody>
    </table>
</div>
<script type="text/javascript">
	$('.edit').on('click',function (){
		var id = $(this).attr('data-id');
        $.ajax({
            url: '{{ route('user-form-edit') }}',
            data: {
                id : id
            },
            type: 'GET',
            beforeSend: function(){
                $('.modal-title').html('Edit User');
                $('.load-data').css('display','block');
                $('.form-data').css('display','none');
            },
            success: function(data){
                $('.load-data').css('display','none');
                $('.form-data').css('display','block');
                $('.form-data').html(data);

            }
        });
	});
	$('.delete').on('click',function (){
        dataid = $(this).attr('data-id');
	});

</script>
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Frontend'], function(){
	
	Route::get('/', 'FrontController@index')->name('front')->middleware('auth.front');

	Route::group(['middleware' => 'auth.only'], function(){
		Route::get('login-page', 'AuthController@loginPage')->name('login-page');
		Route::post('login-api', 'AuthController@loginApi')->name('login-api');
		Route::get('register-page', 'AuthController@registerPage')->name('register-page');
		Route::post('register-api', 'AuthController@registerApi')->name('register-api');
		Route::get('confirmation', 'AuthController@confirmation')->name('confirmation');
	});

	Route::get('logout', 'AuthController@logout')->name('logout');
});
Route::group(['namespace' => 'Backend', 'prefix' => 'panel'], function(){
	Route::group(['middleware' => 'auth.check'], function(){
		Route::get('dashboard', 'DashboardController@index')->name('panel-dashboard');
	});

	Route::group(['prefix' => 'user'], function(){
		Route::group(['middleware' => 'auth.check'], function(){
			Route::get('manage', 'UserController@index')->name('panel-user');
			Route::get('permission', 'PermissionCntrl@index')->name('panel-user-permission');
		});
		
		Route::get('get', 'UserController@get')->name('user-get');
		Route::post('create', 'UserController@create')->name('user-create');
		Route::post('edit', 'UserController@edit')->name('user-edit');
		Route::get('delete', 'UserController@delete')->name('user-delete');
		Route::get('form/create', 'UserController@formCreate')->name('user-form-create');
		Route::get('form/edit', 'UserController@formEdit')->name('user-form-edit');

		Route::get('get-role', 'PermissionCntrl@getRole')->name('role-get');
		Route::post('create-role', 'PermissionCntrl@createRole')->name('role-create');
		Route::post('edit-role', 'PermissionCntrl@editRole')->name('role-edit');
		Route::get('delete-role', 'PermissionCntrl@deleteRole')->name('role-delete');

		Route::get('get-permission', 'PermissionCntrl@getPermission')->name('permission-get');
		Route::post('create-permission', 'PermissionCntrl@createPermission')->name('permission-create');
		Route::post('edit-permission', 'PermissionCntrl@editPermission')->name('permission-edit');
		Route::get('delete-permission', 'PermissionCntrl@deletePermission')->name('permission-delete');

		Route::get('form/role', 'PermissionCntrl@formCreateRole')->name('role-form-create');
		Route::get('form/permission', 'PermissionCntrl@formCreatePermission')->name('permission-form-create');
		Route::get('form/editrole', 'PermissionCntrl@formEditRole')->name('role-form-edit');
		Route::get('form/editpermission', 'PermissionCntrl@formEditPermission')->name('permission-form-edit');
	});

	Route::group(['prefix' => 'website', 'namespace' => 'Website'], function(){
		Route::group(['middleware' => 'auth.check'], function(){
			Route::get('profil', 'ProfilController@index')->name('panel-website-profil');
			Route::get('media', 'MediaController@index')->name('panel-website-media');
		});

		Route::post('profil/update', 'ProfilController@update')->name('panel-website-profil-update');
	});
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

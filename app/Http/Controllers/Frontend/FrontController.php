<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

/**
 * Class HomeController.
 */
class FrontController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
    	$data = [
    		'title' => 'Home'
    	];
        return view('frontend.pages.home', $data);
    }
}

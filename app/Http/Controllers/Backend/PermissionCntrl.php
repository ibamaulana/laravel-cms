<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Helpers\Guzzle;

/**
 * Class HomeController.
 */
class PermissionCntrl extends Controller
{
    public function index()
    {
        $data = [
            'title' => 'Panel User Permission'
        ];
        return view('backend.pages.permission', $data);
    }

    public function getRole()
    {
        $response = $this->guzzle->get([],env('API_URL'),'user/role/get');
        $result = $this->guzzle->getContents($response);

        if ($response->getStatusCode() == '200') {
            $data= [
                'data' => $result->data
            ];

            return view('backend.table.role', $data);
        }

        return view('backend.table.role')->with('error', $result->error->message);
    }

    public function getPermission()
    {
        $response = $this->guzzle->get([],env('API_URL'),'user/permission/get');
        $result = $this->guzzle->getContents($response);
        
        if ($response->getStatusCode() == '200') {
            $data= [
                'data' => $result->data
            ];

            return view('backend.table.permission', $data);
        }

        return view('backend.table.permission')->with('error', $result->error->message);
    }

    public function formCreateRole()
    {
        $reqpermission = $this->guzzle->get([],env('API_URL'),'user/permission/get');
        $permission = $this->guzzle->getContents($reqpermission);

        return view('backend.form.role', ['permission' => $permission]);
    }

    public function formCreatePermission()
    {
        return view('backend.form.permission');
    }

    public function formEditRole(Request $request)
    {
        $reqpermission = $this->guzzle->get([],env('API_URL'),'user/permission/get');
        $permission = $this->guzzle->getContents($reqpermission);

        $rolepermission = $this->guzzle->get(['id' => $request->id],env('API_URL'),'user/role/get-permission');
        $resultrole = $this->guzzle->getContents($rolepermission);

        $data = [
            'permission' => $permission,
            'rolepermission' => $resultrole
        ];

        return view('backend.form.editrole', $data);
    }

    public function formEditPermission(Request $request)
    {
        $reqpermission = $this->guzzle->get([],env('API_URL'),'user/permission/get');
        $permission = $this->guzzle->getContents($reqpermission);

        $rolepermission = $this->guzzle->get(['id' => $request->id],env('API_URL'),'user/role/get-permission');
        $resultrole = $this->guzzle->getContents($rolepermission);

        $data = [
            'permission' => $permission,
            'rolepermission' => $resultrole
        ];

        return view('backend.form.editrole', $data);
    }

    public function createRole(Request $request)
    {
        $response = $this->guzzle->post($request->all(),env('API_URL'),'user/role/create');
        $result = $this->guzzle->getContents($response);

        $role = $this->guzzle->post(['role_name' => $request->role_name, 'permission_name' => $request->permission],env('API_URL'),'user/role/assign-permission');

        $data = [
            'data' => $result
        ];

        return $data;
    }

    public function editRole(Request $request)
    {
        $response = $this->guzzle->post($request->all(),env('API_URL'),'user/role/update');
        $result = $this->guzzle->getContents($response);

        $role = $this->guzzle->post(['role_name' => $request->role_name, 'permission_name' => $request->permission],env('API_URL'),'user/role/assign-permission');

        $data = [
            'data' => $result
        ];

        return $data;
    }

    public function deleteRole(Request $request)
    {
        $response = $this->guzzle->post($request->all(),env('API_URL'),'user/role/delete');
        $result = $this->guzzle->getContents($response);

        $data = [
            'data' => $result
        ];

        return $data;
    }

    public function createPermission(Request $request)
    {
        $response = $this->guzzle->post($request->all(),env('API_URL'),'user/permission/create');
        $result = $this->guzzle->getContents($response);

        $data = [
            'data' => $result
        ];

        return $data;
    }

    public function deletePermission(Request $request)
    {
        $response = $this->guzzle->post($request->all(),env('API_URL'),'user/permission/delete');
        $result = $this->guzzle->getContents($response);

        $data = [
            'data' => $result
        ];

        return $data;
    }
}

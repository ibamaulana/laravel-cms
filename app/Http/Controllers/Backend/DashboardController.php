<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

/**
 * Class HomeController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
    	$data = [
    		'title' => 'Panel Dashboard'
    	];
        return view('backend.pages.dashboard', $data);
    }
}
